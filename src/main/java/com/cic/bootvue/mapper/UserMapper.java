package com.cic.bootvue.mapper;

import com.cic.bootvue.pojo.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserMapper extends JpaRepository<User,Integer>{
    User getByUserNameAndPassWord(String userName,String passWord);
}
