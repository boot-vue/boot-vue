package com.cic.bootvue.controller;

import com.cic.bootvue.pojo.User;
import com.cic.bootvue.result.Result;
import com.cic.bootvue.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import java.util.Objects;

@Controller
public class LoginController {
    @Autowired
    private UserService userService;
    @CrossOrigin
    @PostMapping("api/login")
    @ResponseBody
    public Result login(@RequestBody User user){
        User userFind = userService.findUser(user.getUserName(), user.getPassWord());
        if (null == userFind) {
            return new Result(400);
        } else {
            return new Result(200);
        }
    }

}
