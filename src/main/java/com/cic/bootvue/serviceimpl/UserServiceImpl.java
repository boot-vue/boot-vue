package com.cic.bootvue.serviceimpl;

import com.cic.bootvue.mapper.UserMapper;
import com.cic.bootvue.pojo.User;
import com.cic.bootvue.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper UserMapper;
    @Override
    public User findUser(String userName, String passWord){
       User user= UserMapper.getByUserNameAndPassWord(userName,passWord);
       return user;
    }
}
