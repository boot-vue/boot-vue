package com.cic.bootvue.service;

import com.cic.bootvue.pojo.User;

public interface UserService {
    public User findUser(String userName, String passWord);
}
